const form = document.querySelector('#form');
const questionLabel = document.querySelector('#question');
const endMessage = document.querySelector('#endMessage');

const answerA = document.querySelector('#answerA');
const answerB = document.querySelector('#answerB');
const answerC = document.querySelector('#answerC');
const answerD = document.querySelector('#answerD');
const answers = [answerA, answerB, answerC, answerD];

const circle1 = document.querySelector('#circle1');
const circle2 = document.querySelector('#circle2');
const circle3 = document.querySelector('#circle3');
const circle4 = document.querySelector('#circle4');
const circle5 = document.querySelector('#circle5');
const circle6 = document.querySelector('#circle6');
const circle7 = document.querySelector('#circle7');
const circle8 = document.querySelector('#circle8');
const circle9 = document.querySelector('#circle9');
const circle10 = document.querySelector('#circle10');
const circles = [circle1, circle2, circle3, circle4, circle5, circle6, circle7, circle8, circle9, circle10];
const reload = document.querySelector('#reload');

//décompte et affichage du numéro des questions
let questionNb = 1;

//décompte et affichage du compteur de bonnes réponses
let correctAnswerCounter = 0;
let correctAnswerSpan = document.querySelector('#counter');

//index pour parcourir le tableau de questions
let questionIndex = 0;

//display
let startDiv = document.querySelector('#start');
let startButton = document.querySelector('#startButton');
let quizzDiv = document.querySelector('#quizz');
let endDiv = document.querySelector('#end');

//tableau questions
const myQuestions = [{
    question: "En 2018, quelle est la part du numérique dans les émissions de gaz à effet de serre mondiales ?",
    answers: {
      a: "0,2 %",
      b: "0,7 %",
      c: "3,5 %",
      d: '12 %'
    },
    correctAnswer: "c"
  },
  {
    question: "Quel accord mondial fixe comme objectif la limitation du réchauffement climatique entre 1,5 et 2 °C d'ici 2100 ?",
    answers: {
      a: "Les Accords de Pékin",
      b: "Les Accords de Paris",
      c: "Le traité de Varsovie",
      d: 'Les Accords de Bruxelles'
    },
    correctAnswer: "b"
  },
  {
    question: "Combien de km une donnée numérique (mail, téléchargement, vidéo) parcourt-elle en moyenne ?",
    answers: {
      a: "1200 km",
      b: "3000 km",
      c: "5000 km",
      d: "15 000 km"
    },
    correctAnswer: "d"
  },
  {
    question: "Combien de kilos de matières premières faut-il mobiliser pour fabriquer un ordinateur de 2 kg ?",
    answers: {
      a: "30 kg",
      b: "70 kg",
      c: "220 kg",
      d: '600 kg'
    },
    correctAnswer: "d"
  },
  {
    question: "A quoi équivaut le simple envoi d’un mail d’1 mégaoctet (1 Mo) en consommation électrique ?",
    answers: {
      a: "Une ampoule de 60 watts allumée pendant 25 minutes",
      b: "L'impression de 5 feuilles A4 noir et blanc",
      c: "Un trajet de 1 km en trottinette électrique",
      d: 'Une partie sur PC fixe sur League of Legend'
    },
    correctAnswer: "a"
  },
  {
    question: "Combien consomment les box internet à elles seules en France ?",
    answers: {
      a: "0,05 % de la consommation électrique française annuelle",
      b: "1 % de la consommation électrique française annuelle",
      c: "6 % de la consommation électrique française annuelle",
      d: 'La production annuelle d\'une centrale thermique'
    },
    correctAnswer: "b"
  },
  {
    question: "En France, à combien équivalait en 2015 la consommation des data centers ?",
    answers: {
      a: "La consommation électrique de Strasbourg (280 000 personnes)",
      b: "La consommation électrique de Charvieu (10 000 personnes)",
      c: "La consommation électrique de Lyon (515 000 personnes)",
      d: 'La consommation électrique de l\'Islande (350 000 personnes)'
    },
    correctAnswer: "c"
  },
  {
    question: "A la production, combien consomme un écran TV en équivalent CO2 ? ",
    answers: {
      a: "Un trajet de Bordeaux à Lyon en SUV",
      b: "Un aller-retour Paris-Nice en avion ",
      c: "Un trajet en train transsibérien",
      d: 'L\'ensemble du trafic lyonnais sur une heure '
    },
    correctAnswer: "b"
  },
  {
    question: "Où vont 75 % des déchets électroniques dans le monde ?",
    answers: {
      a: "Dans les filières de recyclage pour récupérer les matières premières, notamment en Europe",
      b: "Dans des décharges à ciel ouvert en Chine, en Inde ou en Afrique principalement",
      c: "Dans les océans, notamment l'Océan Indien",
      d: 'Brûlés dans des filières contrôlés pour éviter un volume trop important en décharge'
    },
    correctAnswer: "b"
  },
  {
    question: "Et en tant que développeur ou développeuse web, que puis-je faire ?",
    answers: {
      a: "Réfléchir à une architecture de site web légère et optimisée",
      b: "Compresser et limiter la taille des feuilles de style CSS, des images, des vidéos et des javascript",
      c: "Ne conserver en historique que les données purement nécessaires",
      d: 'Etre compatible avec d\'anciens terminaux et systèmes d\'exploitation pour ne pas inciter à consommer du neuf'
    },
    correctAnswer: "abcd"
  }
];

//bouton de départ
startButton.addEventListener('click', () => {
  quizzDiv.style.display = 'block';
  startDiv.style.display = 'none';
  buildQuizz(questionIndex);
});

//boutons de réponse
answerA.addEventListener('click', (event) => {
  event.preventDefault();
  checkValue('a', answerA);
});
answerB.addEventListener('click', (event) => {
  event.preventDefault();
  checkValue('b', answerB);
});
answerC.addEventListener('click', (event) => {
  event.preventDefault();
  checkValue('c', answerC);
});
answerD.addEventListener('click', (event) => {
  event.preventDefault();
  checkValue('d', answerD);
});

reload.addEventListener('click', (event) => {
  event.preventDefault();
  document.location.reload();
})

//vérification de la réponse
function checkValue(value, answer) {
  if (myQuestions[questionIndex].correctAnswer.includes(value)) {
    correctAnswerCounter++;
    answer.style.background = "rgba(146, 212, 163, 0.9)";
    circles[questionIndex].style.background = "rgb(115, 205, 138)";
    if (questionIndex===myQuestions.length-1) {
      for (let everyAnswer of answers) {
        everyAnswer.style.background = "rgba(146, 212, 163, 0.9)";
      }
    }
  } else {
    answer.style.background = "rgba(255, 176, 147, 0.9)";
    getCorrectAnswer().style.background = "rgba(146, 212, 163, 0.9)";
    circles[questionIndex].style.background = "rgb(255, 121, 72)";
  }
  toggleButton(true);
  hoverBtnOff();
  questionNb++;
  questionIndex++;
  window.setTimeout(buildQuizz, 1000, questionIndex);
  window.setTimeout(toggleButton, 1000, false);
  window.setTimeout(hoverBtnOn, 1000);
  window.setTimeout(() => {
    for (let answer of answers) {
      answer.style.background = 'rgba(255, 255, 255, 0.9)'
    }
  }, 1000)
}

//retourne la bonne réponse pour pouvoir l'afficher en vert
function getCorrectAnswer() {
  if (myQuestions[questionIndex].correctAnswer === 'a') {
    return answerA
  } else if (myQuestions[questionIndex].correctAnswer === 'b') {
    return answerB
  } else if (myQuestions[questionIndex].correctAnswer === 'c') {
    return answerC
  } else {
    return answerD
  }
}

//active ou désactive l'over sur les réponses
function hoverBtnOff() {
  for (let answer of answers) {
    answer.classList.remove('hoverBtn');
  }
}

function hoverBtnOn() {
  for (let answer of answers) {
    answer.classList.add('hoverBtn');
  }
}

//rend les boutons non clicables
function toggleButton(bool) {
  for (let answer of answers) {
    answer.disabled = bool;
  }
}

/**
 * construction du quizz
 * @param {number} index 
 */
function buildQuizz(index) {
  if (index < myQuestions.length) {
    questionLabel.textContent = questionNb + '/10 : ' + myQuestions[index].question;
    answerA.textContent = myQuestions[index].answers.a;
    answerB.textContent = myQuestions[index].answers.b;
    answerC.textContent = myQuestions[index].answers.c;
    answerD.textContent = myQuestions[index].answers.d;
  } else {
    quizzDiv.style.display = 'none';
    endDiv.style.display = 'block';
    correctAnswerSpan.textContent = correctAnswerCounter;
    if (correctAnswerCounter === 10) {
      endMessage.textContent = 'Parfait! Vous êtes incollable!';
    } else if (correctAnswerCounter >= 7) {
      endMessage.textContent = 'Très bien! Vous êtes plutôt bien renseigné.';
    } else if (correctAnswerCounter >= 4) {
      endMessage.textContent = 'Correct, mais peut mieux faire!';
    } else if (correctAnswerCounter >= 1) {
      endMessage.textContent = 'N\'hésitez pas à refaire ce quiz pour vous améliorer!';
    } else {
      endMessage.textContent = 'Fais un effort frérot'
    }
  }
}

