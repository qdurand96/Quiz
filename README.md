# Quiz

Quiz sur le thème de l'impact écologique du numerique. J'ai utilisé un tableau d'objets, chacun contenant la question, les réponses et la bonne réponse, que je parcours grâce à une fonction.

![alt text](public/img/Wireframe.png)

**Lien:** <https://qdurand96.gitlab.io/Quiz>
